var spawn = require('child_process').spawn;
var debug = require('debug')('munch');
var execSync = require('child_process').execSync;
var http = require('http');
var https = require('https');
var tmp = require('tmp');
var path = require('path');
var fs = require('fs');

const cdndsn = 'user_bd2k797i@push-24.cdn77.com:/www/';
const pass = '4y3Fc0n18okQq97wmVRv';
//const cdnurl = 'http://cdn.clipgif.com/'
const cdnurl = 'https://1495760057.rsc.cdn77.org/';

//Convert input file into a series of stills
var exports = module.exports = {}

exports.download = function ( url, cb ) {
	var agent=http;
	if (url.includes('https')) agent = https;
	var request = agent.get( url , function(response) {
		var pathInv = path.parse( response.req.path );
		var tmpobj = tmp.tmpNameSync({
			postfix:pathInv.ext,
		});
		var stream = response.pipe( fs.createWriteStream( tmpobj ) ).on('close', function() {
			cb(null,tmpobj);
		});
	});
}

exports.analyze = function (file, cb) {
	var buffer = '';
	var ffprobe = execSync('ffprobe -v quiet -print_format json -show_streams '+file);
	var ffdata = JSON.parse( ffprobe.toString() )['streams'][0];
	var duration = ffdata['duration'] * 1000;
	var height = ffdata['height'];
	var width = ffdata['width'];
	cb(null,[duration,height,width]);
}

exports.transport = function( file, cb ) {
	var calcSum='';
	var sum = spawn('md5sum',[
		file
	]);
	sum.stdout.on('data', function( data ) {
		calcSum = String(data).split(' ')[0];
		var fileInfo = path.parse( file );
	});
	sum.on('close', function() {
		exports.analyze( file , function(err, results) {
			var dir = calcSum.substring(0,2);
			var mp4 = dir+'/'+calcSum+'.mp4';
			var png = dir+'/'+calcSum+'.png';
			console.log(results);
			//Move file to CDN
			var trans = spawn('rsync',[
				'-vaR',
				file,
				'user_bd2k797i@push-24.cdn77.com:/www/'+mp4
			]);
			trans.on('close', function( code ) {
				var fresults = {
					url:cdnurl,
					mp4:mp4,
					png:png,
					duration:results[0],
					height:results[1],
					width:results[2]
				}
				cb(null,fresults);
			});
			//Insert MySQL record into webserver
			//Insert MySQL record into localmachine
		});
	});
}

exports.munch = function ( url , cb) {
	var urlParts = path.parse(url);
	if (urlParts['ext'] == '.gifv') {
		url = [urlParts['dir'],'/',urlParts['name'],'.mp4'].join('');
	}
	//Download remote media.
	exports.download( url, function( err, file ) {
		//convert file to pngs
		var outobj = tmp.tmpNameSync({
			postfix:'.mp4',
		});
	
		var conversion = execSync('ffmpeg -i '+file+' '+outobj, { stdio:['pipe','pipe','ignore'] });
		fs.unlink(file);
		exports.transport( outobj , function(err,results) {
			fs.unlink( outobj );
			return cb(null,results);
		});
	});
}

