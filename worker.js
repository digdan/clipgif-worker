#!/bin/env node
var bull = require('bull');
var cluster = require('cluster');
var gifMunch = require('./index.js');
var munchQueue = bull('video',6379,'216.189.156.111',{password:'zeR0Zer0aBc',database:'0'});
const numWorkers = 2;

if (cluster.isMaster) {
	var debug = require('debug')('munchMaster');
	debug('Initializing workers');
	for( var i = 0; i < numWorkers; i++) {
		debug('Forking worker #'+i);
		cluster.fork();
	}
} else {
	var debug = require('debug')('munchWorker '+cluster.worker.id);
	debug('initiated.');
	munchQueue.process(function(job,done) {
		var data = JSON.parse(job.data);
		gifMunch.munch(data['url'], function(err,mdata) {
			debug(err,mdata);
			if (err) done(err);
			mdata['id'] = data['id'];
			done(null,mdata);
		});
	});
}
